var LOG4JS = require("log4js");

Config = {};

Config.database = {
    host: "127.0.0.1",
    port: 3306,
    user: "root",
    password: "0827",
    database: "node_web"
};

Config.logConfig = {
    appenders: [
        {                                       // 需要控制台打印
            type: "console"
        },
        {                                       // normal_log 记录器
            type: "dateFile",                   // 日志文件类型，可以使用日期作为文件名的占位符
            filename: "log/",
            pattern: "normal-yyyy-MM-dd.log",
            alwaysIncludePattern: true,
            category: "normal"
        }
    ],
    levels: {                                   // 设置日志级别，低于该级别的将不会被打印到控制台
        normal: "trace"
    }
};

LOG4JS.configure(Config.logConfig);

exports.Config = Config;
exports.Logger = LOG4JS.getLogger("normal");