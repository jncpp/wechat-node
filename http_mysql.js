var UTIL = require("util");
var FS = require("fs");
var CONFIG = require("./config.js").Config;
var LOGGER = require("./config.js").Logger;
var MYSQL = require("mysql");   // api: https://github.com/felixge/node-mysql
// 对mysql模块，node.js的版本不能低于0.8，否则可能出现connect ECONNREFUSED的错误

var pool = MYSQL.createPool(CONFIG.database);

var Query = function(sql, values, cb){
    pool.getConnection(function(err, connection){
        var query = MYSQL.createQuery(sql, values, cb);
        if(err){
            LOGGER.error("get mysql pool connection error: %s", err.message);
            query_list.push(query);
        }else{
            query.once("end", function () {
                connection.release();
            });
            connection.query(query);
        }
    });
};

exports.Query = Query;
//exports.InsertCharge = Insert_Charge;